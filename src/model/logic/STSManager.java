package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import model.data_structures.DoubleLinkedList;
import model.data_structures.LPHashTable;
import model.data_structures.RedBlackBsT;
import model.data_structures.SeparateChainingHashTable;




public class STSManager {
	
	private PersistenceManager persistencia;
	private LPHashTable<Integer, VOTrip> trips;
	private SeparateChainingHashTable<Integer,VOStop> stops;

	public void ITSInit() 
	{
		

		persistencia = new PersistenceManager();
		trips = persistencia.loadTrips("./data/trips.txt");
		stops = persistencia.loadStops("./data/stops.txt");
		loadStopTimes("./data/stop_times.txt");


	}
	//Da un arbol balanceado con los	viajes	que	paran	en	la	parada dada 
	
	public RedBlackBsT<Date, VOHoraParadaPorParada> darviajesParadaPorHora(int stopId) throws Exception
	{
		RedBlackBsT<Date, VOHoraParadaPorParada> arbol = new RedBlackBsT<Date,VOHoraParadaPorParada>();
		Iterator<VOHoraParadaPorParada> ite = stops.get(stopId).getTiemposViaje().iterador();
		while(ite.hasNext()) 
		{
			VOHoraParadaPorParada actual = ite.next();
			Date horaActual = parsingDate(actual.getArrivalTime());
			arbol.put(horaActual, actual);
		}
		
		return arbol;
	}
	
	 //retorna	 todas	 las	 rutas	 y	 sus	 respectivos	 viajes	que	paran	en	un	rango	de	horas	especificado	para	una	parada	dada
	
	public DoubleLinkedList<VOHoraParadaPorParada> paradasRangoHora (int parada , String HoraInicio, String HoraFin ) throws Exception
	{
		
		RedBlackBsT<Date, VOHoraParadaPorParada> arbol = darviajesParadaPorHora(parada);
		DoubleLinkedList<VOHoraParadaPorParada> lista = new DoubleLinkedList<VOHoraParadaPorParada>();
		Date inicio = parsingDate(HoraInicio);
		Date fin = parsingDate(HoraFin);
		Iterator<VOHoraParadaPorParada> iterador = arbol.valuesInRange(inicio, fin);
		while(iterador.hasNext()) 
		{
			lista.add(iterador.next());
		}
		return lista;

		
	}
	
	public Date parsingDate(String date) throws ParseException 
	{	
		SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss a");
		DateFormat formatMilitar = new SimpleDateFormat( "HH:mm:ss");
		Date time = null;

		try 
		{
			time = format.parse(date);
		} catch (ParseException e) {

			time = formatMilitar.parse(date);	 
		}

		if (time != null) 
		{
			String formattedDate = formatMilitar.format(time);
		}

		return time;

	}
	
	
	public void loadStopTimes(String stopTimesFile) {


		String cadena;

		FileReader file = null;
		BufferedReader reader = null;
		String datos[];

		try
		{
			file= new FileReader(stopTimesFile);
			reader = new BufferedReader(file);

			boolean firstHeaderRow = true;

			while((cadena = reader.readLine())!=null)
			{
				if(!firstHeaderRow)
				{
					datos = cadena.split(",");

					int tripId = Integer.parseInt(datos[0]);
					String arrivalTime = persistencia.getStringFromCsvLine(datos, 1);
					String departureTime = persistencia.getStringFromCsvLine(datos, 2);
					int stopId = persistencia.getIntFromCsvLine(datos, 3);


					VOHoraParadaPorParada viaje = new VOHoraParadaPorParada(tripId, arrivalTime, departureTime );
					
					viaje.setIdRoute(trips.get(tripId).getRouteId());

					
					stops.get(stopId).getTiemposViaje().put(viaje.hashCode(), viaje);

				}
				else
				{
					firstHeaderRow = false;
				}
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("No se pudo cargar el archivo. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if(file != null)
				{
					file.close();
				}

				if(reader != null)
				{
					reader.close();
				}
			}
			catch(Exception e2)
			{
				throw new RuntimeException("Error al cerrar el archivo. " + e2.getMessage(), e2);
			}
		}

	}

}
