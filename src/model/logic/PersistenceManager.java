package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import model.data_structures.LPHashTable;
import model.data_structures.SeparateChainingHashTable;





public class PersistenceManager 
{

	


	public PersistenceManager() {


	}

	public String getStringFromCsvLine(String[] csvLineFields, int index) {
		return csvLineFields[index].trim();
	}

	public int getIntFromCsvLine(String[] csvLineFields, int index) {
		return Integer.parseInt(getStringFromCsvLine(csvLineFields, index));
	}

	public Integer getNullableIntFromCsvLine(String[] csvLineFields, int index) {
		int number = 0;
		try 
		{
			number = getStringFromCsvLine(csvLineFields, index).isEmpty() ? null : getIntFromCsvLine(csvLineFields, index);
		} catch (Exception e) {

		}
		return number;
	}


	


	public LPHashTable<Integer, VOTrip> loadTrips(String tripsFile) {

		LPHashTable<Integer, VOTrip> trips = new  LPHashTable<Integer, VOTrip>();
		String cadena;

		FileReader file = null;
		BufferedReader reader = null;

		try
		{
			file= new FileReader(tripsFile);
			reader = new BufferedReader(file);

			boolean firstHeaderRow = true;

			while((cadena = reader.readLine())!=null)
			{
				if(!firstHeaderRow)
				{
					String csvLineFields[] = cadena.split(",");

					int routeId = getIntFromCsvLine(csvLineFields, 0);
					int serviceId = getIntFromCsvLine(csvLineFields, 1);
					int tripId = getIntFromCsvLine(csvLineFields, 2);
					
					String tripShortName = getStringFromCsvLine(csvLineFields, 4);
					

					VOTrip newTrip = new VOTrip(routeId, serviceId, tripId, tripShortName);

					trips.put(newTrip.hashCode(), newTrip); // al final para preservar el orden del archivo
				}
				else
				{
					firstHeaderRow = false;
				}
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("No se pudo cargar el archivo. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if(file != null)
				{
					file.close();
				}

				if(reader != null)
				{
					reader.close();
				}
			}
			catch(Exception e2)
			{
				throw new RuntimeException("Error al cerrar el archivo. " + e2.getMessage(), e2);
			}
		}
		return trips;
	}


	


	public SeparateChainingHashTable<Integer,VOStop> loadStops(String stopsFile) {
		String cadena;

		SeparateChainingHashTable<Integer,VOStop> stops = new SeparateChainingHashTable<Integer,VOStop>();

		FileReader file = null;
		BufferedReader reader = null;
		String datos[] = null;

		try
		{
			file= new FileReader(stopsFile);
			reader = new BufferedReader(file);

			boolean firstHeaderRow = true;

			while((cadena = reader.readLine())!=null)
			{
				if(!firstHeaderRow)
				{
					datos = cadena.split(",");

					int stopId = getIntFromCsvLine(datos, 0);
					Integer stopCode = getNullableIntFromCsvLine(datos, 1);
					String stopName = getStringFromCsvLine(datos, 2);
					
					double stopLat = Double.parseDouble(getStringFromCsvLine(datos, 4));
					double stopLon = Double.parseDouble(getStringFromCsvLine(datos, 5));
					

					VOStop newStop = new VOStop(stopId, stopCode, stopName, stopLat, stopLon);


					stops.put(newStop.hashCode(), newStop);
				}
				else
				{
					firstHeaderRow = false;
				}
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("No se pudo cargar el archivo. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if(file != null)
				{
					file.close();
				}

				if(reader != null)
				{
					reader.close();
				}
			}
			catch(Exception e2)
			{
				throw new RuntimeException("Error al cerrar el archivo. " + e2.getMessage(), e2);
			}
		}
		return stops;
	}


}
