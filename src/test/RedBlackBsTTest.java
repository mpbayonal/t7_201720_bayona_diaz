package test;

import junit.framework.TestCase;
import model.data_structures.RedBlackBsT;

public class RedBlackBsTTest extends TestCase {
	
	private RedBlackBsT<Integer, Double> tree;

	protected void setUp() throws Exception {
		tree= new RedBlackBsT<Integer, Double>();
	}

	public void testGetK() {
		
	
		
		tree= new RedBlackBsT<Integer, Double>();
		for (int i = 0; i < 20; i++) 
		{
			try 
			{
				tree.put(i, i*2.0);
			} catch (Exception e) 
			{
				
			}
			
			try 
			{
				assertEquals("el elemento no es correto", i * 2.0 , tree.get(i));
			} catch (Exception e) 
			{
				System.out.println("Error al acceder al objeto");
			}
		
		}
		
	
	}
	
	public void putTest() {
		tree= new RedBlackBsT<Integer, Double>();
		for (int i = 0; i < 500; i++) {
			try {
				tree.put(i*2, i*1.0);
			} catch (Exception e) {
				System.out.println("No se pudieron agrear los elementos");
			}
			
			
		}
		
		try {
			tree.put(1, 1.0);
			assertEquals("el elemento no es correto", 1.0, tree.get(1));
		} catch (Exception e) {
			System.out.println("Error al acceder al objeto");
		}
		
	}

	public void testContains() {
		
		tree= new RedBlackBsT<Integer, Double>();
		for (int i = 0; i < 500; i++) {
			try {
				tree.put(i*2, i*1.0);
			} catch (Exception e) {
				System.out.println("No se pudieron agrear los elementos");
			}
			
			
		}
		
		try {
			assertEquals("el elemento no es correto", true, tree.contains(2));
		} catch (Exception e) {
			System.out.println("Error al acceder al objeto");
		}
	}
	
	public void deleteTest() throws Exception {
		tree= new RedBlackBsT<Integer, Double>();
		for (int i = 0; i < 500; i++) {
			try {
				tree.put(i*2, i*1.0);
			} catch (Exception e) {
				System.out.println("No se pudieron agrear los elementos");
			}
			
			
		}
		
		try {
			tree.delete(4);
			assertEquals("el elemento no es correto", null, tree.contains(4));
		} catch (Exception e) {
			System.out.println("Error al acceder al objeto");
		}
	}
	
	
	public void sizeTest() {
		tree= new RedBlackBsT<Integer, Double>();
		for (int i = 0; i < 500; i++) {
			try {
				tree.put(i*2, i*1.0);
			} catch (Exception e) {
				System.out.println("No se pudieron agrear los elementos");
			}
			
			
		}
		assertEquals("el elemento no es correto", 500, tree.size());
	}
	public void isEmptyTest() {
		tree= new RedBlackBsT<Integer, Double>();
		for (int i = 0; i < 20; i++) {
			try {
				tree.put(i, i*2.0);
			} catch (Exception e) {
				System.out.println("No se pudieron agrear los elementos");
			}
			
		}
		assertEquals("el elemento no es correto", false, tree.isEmpty());
	}
	

}
